package sample.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;

import java.net.URL;
import java.util.ResourceBundle;

public class LoginController {
    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private JFXButton loginRegistrateButton;

    @FXML
    private JFXButton loginEnterButton;

    @FXML
    private JFXTextField loginUserName;

    @FXML
    private JFXTextField loginPassword;

    @FXML
    void initialize() {

        loginEnterButton.setOnAction( event -> {
            System.out.println("Clicked");

        } );
    }
}
